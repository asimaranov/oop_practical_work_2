#include <string>
#include <map>
#include <tuple>
#include <iostream>
#include <queue>
#include <cmath>

using namespace std;

template<class K, class V>
tuple<K, V> find_by_key(map<K, V> map, K key){
    return make_tuple(key, map[key]);
}

template<class K, class V>
tuple<K, V> find_by_value(map<K, V> map, V value){
    for (auto it = map.begin(); it != map.end(); ++it)
        if (it->second == value)
            return make_tuple(it->first, value);

}

template<class K, class V>
void print(map<K, V> map){
    auto iter = map.begin();
    while (iter != map.end())
    {
        cout << "Key: " << iter->first << ", value: " << iter->second << "\n";
        iter++;
    }
}

template<class K, class V>
 map<K, V> filter(map<K, V> m, std::function<bool(V &)> predicate) {

    auto result_map = map < K, V>();
    auto iter = m.begin();

    while (iter != m.end()) {
        if (predicate(iter->second)) {
            result_map.template insert(std::make_pair(iter->first, iter->second));
        }
        iter++;
    }
    return result_map;

}


template<typename T>
void print_queue(T& q) {
    while (!q.empty()) {
        cout << q.top() << " ";
        q.pop();
    }
    std::cout << '\n';
}
template<typename T>
void print_queue_desc(T& q) {
    if (!q.empty()) {
        auto top = q.top();
        q.pop();
        print_queue_desc(q);
        cout << top << " ";
    }
    std::cout << '\n';
}

class Superhero {
private:
    string nick;
    string real_name;
    string birthday;
    bool is_male;
    string superpower;
    string weaknesses;
    int wins;
    int might;

public:

    Superhero(string &&nick, string &&real_name, string &&birthday, bool is_male, string &&superpower,
              string &&weaknesses, int wins, int might) :
            nick(nick),
            real_name(real_name), birthday(birthday), is_male(is_male), superpower(superpower),
            weaknesses(weaknesses), wins(wins), might(might) {


    }

    bool operator>(Superhero const &other) const {
        if (this->might != other.might) return this->might > other.might;
        if (this->wins != other.wins) return this->wins > other.wins;
        if (this->nick != other.nick) return this->nick > other.nick;

        return false;
    }

    bool operator<(Superhero const &other) const {
        if (this->might != other.might) return this->might < other.might;
        if (this->wins != other.wins) return this->wins < other.wins;
        if (this->nick != other.nick) return this->nick < other.nick;

        return false;
    }

    friend std::ostream &operator<<(std::ostream &os, Superhero const &h) {
        return os << h.nick;
    };
    string get_superpower(){return this->superpower;}
};


template<class T>
class Node
{
protected:
//закрытые переменные Node N; N.data = 10 вызовет ошибку
    T data;
//не можем хранить Node, но имеем право хранить указатель
    Node* left;
    Node* right;
    Node* parent;
//переменная, необходимая для поддержания баланса дерева
    int height;
public:
//доступные извне переменные и функции
    virtual void setData(T d) { data = d; }
    virtual T getData() { return data; }
    int getHeight() { return height; }
    virtual Node* getLeft() { return left; }
    virtual Node* getRight() { return right; }
    virtual Node* getParent() { return parent; }
    virtual void setLeft(Node* N) { left = N; }
    virtual void setRight(Node* N) { right = N; }
    virtual void setParent(Node* N) { parent = N; }
//Конструктор. Устанавливаем стартовые значения для указателей
    Node<T>(T n)
    {
        data = n;
        left = right = parent = NULL;
        height = 1;
    }
    Node<T>()
    {
        left = NULL;
        right = NULL;
        parent = NULL;
        data = 0;
        height = 1;
    }
    virtual void print()
    {
        cout << "\n" << data;
    }
    virtual void setHeight(int h)
    {
        height = h;
    }
    friend ostream& operator<< (ostream& stream, Node<T>& N);
};
template<class T>
ostream& operator<< (ostream& stream, Node<T>& N)
{
    stream << "\nNode data: " << N.data << ", height: " << N.height;
    return stream;
}
template<class T>
void print(Node<T>* N) { cout << "\n" << N->getData(); }
template<class T>
class Tree
{
protected:
//корень - его достаточно для хранения всего дерева
    Node<T>* root;
public:
//доступ к корневому элементу
    virtual Node<T>* getRoot() { return root; }
//конструктор дерева: в момент создания дерева ни одного узла нет, корень смотрит в никуда
    Tree<T>() { root = NULL; }
//рекуррентная функция добавления узла. Устроена аналогично, но вызывает сама себя - добавление в левое или правое поддерево
    virtual Node<T>* Add_R(Node<T>* N)
    {
        return Add_R(N, root);
    }
    virtual Node<T>* Add_R(Node<T>* N, Node<T>* Current)
    {
        if (N == NULL) return NULL;
        if (root == NULL)
        {
            root = N;
            return N;
        }
        if (Current->getData() > N->getData())
        {
//идем влево
            if (Current->getLeft() != NULL)
                Current->setLeft(Add_R(N, Current->getLeft()));
            else
                Current->setLeft(N);
            Current->getLeft()->setParent(Current);
        }
        if (Current->getData() < N->getData())
        {
//идем вправо
            if (Current->getRight() != NULL)
                Current->setRight(Add_R(N, Current->getRight()));
            else
                Current->setRight(N);
            Current->getRight()->setParent(Current);
        }
        if (Current->getData() == N->getData())
//нашли совпадение
                ;
//для несбалансированного дерева поиска
        return Current;
    }
//функция для добавления числа. Делаем новый узел с этими данными и вызываем нужную функцию добавления в дерево
    virtual void Add(int n)
    {
        Node<T>* N = new Node<T>;
        N->setData(n);
        Add_R(N);
    }
    virtual Node<T>* Min(Node<T>* Current=NULL)
    {
//минимум - это самый "левый" узел. Идём по дереву всегда влево
        if (root == NULL) return NULL;
        if(Current==NULL)
            Current = root;
        while (Current->getLeft() != NULL)
            Current = Current->getLeft();
        return Current;
    }
    virtual Node<T>* Max(Node<T>* Current = NULL)
    {
//минимум - это самый "правый" узел. Идём по дереву всегда вправо
        if (root == NULL) return NULL;
        if (Current == NULL)
            Current = root;
        while (Current->getRight() != NULL)
            Current = Current->getRight();
        return Current;
    }
//поиск узла в дереве. Второй параметр - в каком поддереве искать, первый - что искать
    virtual Node<T>* Find(int data, Node<T>* Current)
    {
//база рекурсии
        if (Current == NULL) return NULL;
        if (Current->getData() == data) return Current;
//рекурсивный вызов
        if (Current->getData() > data) return Find(data, Current->getLeft());
        if (Current->getData() < data) return Find(data, Current->getRight());
    }
//три обхода дерева
    virtual void PreOrder(Node<T>* N, void (*f)(Node<T>*))
    {
        if (N != NULL)
            f(N);
        if (N != NULL && N->getLeft() != NULL)
            PreOrder(N->getLeft(), f);
        if (N != NULL && N->getRight() != NULL)
            PreOrder(N->getRight(), f);
    }
//InOrder-обход даст отсортированную последовательность
    virtual void InOrder(Node<T>* N, void (*f)(Node<T>*))
    {
        if (N != NULL && N->getLeft() != NULL)
            InOrder(N->getLeft(), f);
        if (N != NULL)
            f(N);
        if (N != NULL && N->getRight() != NULL)
            InOrder(N->getRight(), f);
    }
    virtual void PostOrder(Node<T>* N, void (*f)(Node<T>*))
    {
        if (N != NULL && N->getLeft() != NULL)
            PostOrder(N->getLeft(), f);
        if (N != NULL && N->getRight() != NULL)
            PostOrder(N->getRight(), f);
        if (N != NULL)
            f(N);
    }
};



int main3(){

    auto superman = Superhero("superman", "Unknown", "11.11.2011", true, "supermight", "", 5, 99);
    auto batman = Superhero("batman", "Unknown", "10.10.2010", true, "batmight", "", 4, 100);
    auto catgirl = Superhero("catgirl", "Unknown", "10.10.2010", true, "catmight", "", 0, 5);
    auto badman = Superhero("badman", "Unknown", "10.10.2010", true, "badmight", "", 4, 100);

    map<string, Superhero> superheroes = map<string, Superhero>();
    superheroes.insert(std::make_pair("superman", superman));
    superheroes.insert(std::make_pair("batman", batman));
    superheroes.insert(std::make_pair("catgirl", catgirl));
    superheroes.insert(std::make_pair("badman", badman));

    print<string, Superhero>(filter<string, Superhero>(superheroes, [](Superhero hero) -> bool {
        return hero.get_superpower() > "ca";
    }));

    auto comp = [] (Superhero a, Superhero b) -> bool { return a.get_superpower() > b.get_superpower(); };
    priority_queue<Superhero, std::vector<Superhero>, decltype(comp)> q(comp);
    for (Superhero h : {superman, batman, catgirl, badman})
        q.push(h);

    print_queue(q);


    Tree<double> T;
    int arr[15];
    int i = 0;
    for (i = 0; i < 15; i++) arr[i] = (int)(100 * cos(15 * double(i+1)));
    for (i = 0; i < 15; i++)
        T.Add(arr[i]);
    Node<double>* M = T.Min();
    cout << "\nMin = " << M->getData() << "\tFind " << arr[3] << ": " <<
         T.Find(arr[3], T.getRoot());
    void (*f_ptr)(Node<double>*); f_ptr = print;
    cout << "\n-----\nInorder:";
    T.InOrder(T.getRoot(), f_ptr);
    char c; cin >> c;
    return 0;

}