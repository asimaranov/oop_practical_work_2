#include <iostream>
#include <vector>

using namespace std;

void tree_dfs(int tree[9][9], vector<int> *nodes, bool met[9], int curr_node){

    met[curr_node] = true;
    nodes->push_back(curr_node);

    for(int i = 0; i < 9; i++){

        if(i == curr_node) continue;
        if(tree[curr_node][i] > 0){
            if(!met[i]){
                tree_dfs(tree, nodes, met, i);
            }
        }
    }
}



int mainh()
{

    int graph[9][9] = {
            { 0, 9, 4, 3, 8, 7, 8, 2, 1 },
            { 9, 0, 8, 2, 1, 0, 7, 9, 5 },
            { 4, 8, 0, 8, 4, 4, 7, 5, 5 },
            { 3, 2, 8, 0, 3, 4, 6, 2, 6 },
            { 8, 1, 4, 3, 0, 5, 3, 2, 2 },
            { 7, 0, 4, 4, 5, 0, 5, 2, 6 },
            { 8, 7, 7, 6, 3, 5, 0, 0, 0 },
            { 2, 9, 5, 2, 2, 2, 0, 0, 6 },
            { 1, 5, 5, 6, 2, 6, 0, 6, 0 },
    };

    int tree[9][9] = {0};

    bool met_vertices[9] = {false};

    met_vertices[0] = true;

    int row_id;
    int col_id;

    for(int curr_edge_id = 0; curr_edge_id < 8; curr_edge_id++){
        row_id = 0;
        col_id = 0;
        unsigned min = -1;


        for(int i = 0; i < 9; i++){
            if(met_vertices[i]){
                for(int j = 0; j < 9; j++){
                    if(!met_vertices[j] && graph[i][j]){
                        if (min > graph[i][j]) {
                            min = graph[i][j];
                            row_id = i;
                            col_id = j;
                        }

                    }
                }
            }

        }
        tree[row_id][col_id] = graph[row_id][col_id];
        tree[col_id][row_id] = graph[row_id][col_id];

        met_vertices[col_id] = true;

    }
    cout << "Tree: " << endl;
    for (int i = 0; i < 9; ++i)
    {
        for (int j = 0; j < 9; ++j)
        {
            std::cout << tree[i][j] << ' ';
        }
        std::cout << std::endl;
    }

    auto nodes = new vector<int>();
    bool met[9] = {false};
    tree_dfs(tree, nodes, met, 0);

    for(auto e: *nodes){
        cout << e + 1 << endl;
    }

    return 0;
}