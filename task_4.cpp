// OpenCV_example.cpp
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <cmath>

using namespace cv;
using namespace std;


int kernel[3][3] = {
        {1, 0, 0},
        {0, 0, 0},
        {0, 0, -1}
};

uchar grayscale(Vec<uchar, 3> color) {
    return color[0] * 0.3 + color[1] * 0.59 + color[2] * 0.11;
}

int main() {
// Открываем файл изображения
    Mat3b img = cv::imread("img.png");
    if (img.empty()) // Проверяем, удалось ли загрузить изображение
    {
        cout << "Cannot load image!" << endl; // выводим сообщение в случае ошибки
        return -1;
    }
    // Создаем переменную для выходного изображения того же размера
    auto size = img.size();
    cout << size << endl;
    size.width /= 3;
    size.height /= 3;
    cout << size << endl;

    Mat3b img_conv(size), img_conv_activated(size);

    imshow("img", img); // Открываем окно с изображением

    Point point(23, 42);

    const cv::Vec3b &bgr = img(point); // или img.at<Vec3b>(point);
    cout << bgr << endl;

    for (int y = 0; y < img.rows / 3 - 3; y++) {
        for (int x = 0; x < img.cols / 3 - 3; x++) {


            unsigned char sum = 0;

            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++) {
                    sum += grayscale(img.at<Vec3b>(Point(x * 3 + i, y * 3 + j))) + kernel[i][j];
                }

            Vec3b color = Vec3b(sum, sum, sum);
            auto activated_sum = (unsigned char)(log(1 + exp((double)sum/255)) * 255);
            Vec3b activated_color = Vec3b( activated_sum, activated_sum, activated_sum);

            img_conv.at<Vec3b>(Point(x, y)) = color;
            img_conv_activated.at<Vec3b>(Point(x, y)) = activated_color;

        }
    }
    imshow("img_conv", img_conv);
    imshow("img_conv_activated", img_conv_activated);

    imwrite("../conv_out.jpg", img_conv);
    imwrite("../conv_activated_out.jpg", img_conv_activated);

    waitKey(0);

    return 0;
}