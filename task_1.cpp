#include <iostream>
#include <list>
#include <iostream>
#include <fstream>
#include <utility>

using namespace std;


template<class T>
void push(list<T> &list, T element) {
    auto p = list.begin();
    while (p != list.end()) {
        if (*p > element)
            break;
        p++;
    }
    list.insert(p, element);
}

template<class T>
void push_descending(list<T> &list, T element) {
    auto p = list.begin();
    while (p != list.end()) {
        if (*p < element)
            break;
        p++;
    }
    list.insert(p, element);
}

/*template<class T>
void pop(list<T> &lst, T element) {
    auto p = lst.begin();
    while (p != lst.end()) {
        if (*p == element) {
            lst.erase(p);
            break;
        }
        p++;
    }

}*/
template<class T>
T pop(list<T> &lst) {
    T elem = *lst.begin();
    lst.pop_front();
    return elem;


}

template<class T>
void filter(list<T> &lst, const std::function<bool(const T &)> predicate) {
    auto p = lst.begin();

    while (p != lst.end()) {
        if (!predicate(*p)) {
            lst.erase(p);

        }
        p++;
    }

}

template<class T>
void print(list<T> &lst) {
    auto p = lst.begin();
    while (p != lst.end()) {
        //перемещение по контейнеру с помощью указателя, нет операции [i]
        cout << *p << " ";
        p++;
    }
    cout << endl;

}

class Fraction {
private:
    int numerator, denominator;

public:
    Fraction(int numerator, int denominator) {
        this->numerator = numerator;
        this->denominator = denominator;
    }

    int get_numerator() const {
        return this->numerator;
    }

    int get_denominator() const {
        return this->denominator;
    }
};

std::ostream &operator<<(std::ostream &os, Fraction const &f) {
    return os << f.get_numerator() << "/" << f.get_denominator();
}

class Superhero {
private:
    string nick;
    string real_name;
    string birthday;
    bool is_male;
    string superpower;
    string weaknesses;
    int wins;
    int might;

public:

    Superhero(string &&nick, string &&real_name, string &&birthday, bool is_male, string &&superpower,
              string &&weaknesses, int wins, int might) :
            nick(nick),
            real_name(real_name), birthday(birthday), is_male(is_male), superpower(superpower),
            weaknesses(weaknesses), wins(wins), might(might) {


    }

    bool operator>(Superhero const &other) const {
        if (this->might != other.might) return this->might > other.might;
        if (this->wins != other.wins) return this->wins > other.wins;
        if (this->nick != other.nick) return this->nick > other.nick;

        return false;
    }

    bool operator<(Superhero const &other) const {
        if (this->might != other.might) return this->might < other.might;
        if (this->wins != other.wins) return this->wins < other.wins;
        if (this->nick != other.nick) return this->nick < other.nick;

        return false;
    }

    friend std::ostream &operator<<(std::ostream &os, Superhero const &h) {
        return os << h.nick;
    }
};


template<class T>
class Element {
//элемент связного списка
private:
//указатель на предыдущий и следующий элемент
    Element *next;
    Element *prev;
//информация, хранимая в поле
    T field;
public:
    Element(T value, Element<T> *next_ptr = NULL, Element<T> *prev_ptr = NULL) : field(std::move(value)),
                                                                                 next(next_ptr), prev(prev_ptr) {}

//доступ к полю *next
    virtual Element *getNext() { return next; }

    virtual void setNext(Element *value) { next = value; }

//доступ к полю *prev
    virtual Element *getPrevious() { return prev; }

    virtual void setPrevious(Element *value) {
        prev = value;
    }

//доступ к полю с хранимой информацией field
    virtual T getValue() { return field; }

    virtual void setValue(T value) { field = value; }


    friend ostream &operator<<(ostream &ustream, Element<T> &obj) {
        return ustream;
    };
};

template<class T>
ostream &operator<<(ostream &ustream, Element<T> &obj) {
    ustream << obj.field;
    return ustream;
}

template<class T>
class LinkedListParent {
protected:
//достаточно хранить начало и конец
    Element<T> *head;
    Element<T> *tail;


//для удобства храним количество элементов
    int num;
public:
    virtual int Number() { return num; }

    virtual Element<T> *getBegin() { return head; }

    virtual Element<T> *getEnd() { return tail; }

    LinkedListParent() {
//конструктор без параметров
        cout << "\nParent constructor";
        head = NULL;
        num = 0;
    }

//чисто виртуальная функция: пока не определимся с типом списка, не сможем реализовать добавление

    virtual Element<T> *push(T value) = 0;

//чисто виртуальная функция: пока не определимся с типом списка, не сможем реализовать удаление

    virtual Element<T> *pop() = 0;

    virtual ~LinkedListParent() {
//деструктор - освобождение памяти
        cout << "\nParent destructor";
    }

//получение элемента по индексу - какова асимптотическая оценка этого действия?
    virtual Element<T> *operator[](int i) {
//индексация
        if (i < 0 || i > num) return NULL;
        int k = 0;
//ищем i-й элемент - вставем в начало и отсчитываем i шагов вперед
        Element<T> *cur = head;
        for (k = 0; k < i; k++) {
            cur = cur->getNext();
        }
        return cur;
    }


    friend ostream &operator<<(ostream &ustream, LinkedListParent<T> &obj) {

        if (typeid(ustream).name() == typeid(ofstream).name()) {
            ustream << obj.num << "\n";
            for (Element<T> *current = obj.getBegin(); current != NULL; current =
                                                                                current->getNext())
                ustream << current->getValue() << " ";
            return ustream;
        }

        ustream << "\nLength: " << obj.num << "\n";
        int i = 0;

        for (Element<T> *current = obj.getBegin(); current != NULL; current = current->getNext(), i++)
            ustream << "arr[" << i << "] = " << current->getValue() << "\n";
        return ustream;
    };


    friend istream &operator>>(istream &ustream,
                               LinkedListParent<T> &obj);
};


template<class T>
istream &operator>>(istream &ustream, LinkedListParent<T> &obj) {
//чтение из файла и консоли совпадают
    int len;
    ustream >> len;
//здесь надо очистить память под obj, установить obj.num = 0
    double v = 0;
    for (int i = 0; i < len; i++) {
        ustream >> v;
        obj.push(v);
    }
    return ustream;
}

template<typename ValueType>
class ListIterator : public std::iterator<std::input_iterator_tag, ValueType> {
private:
public:
    ListIterator() { ptr = NULL; }

//ListIterator(ValueType* p) { ptr = p; }
    ListIterator(Element<ValueType> *p) { ptr = p; }

    ListIterator(const ListIterator &it) { ptr = it.ptr; }

    bool operator!=(ListIterator const &other) const { return ptr != other.ptr; }

    bool operator==(ListIterator const &other) const {
        return ptr == other.ptr;
    }//need for BOOST_FOREACH
    Element<ValueType> &operator*() {
        return *ptr;
    }

    ListIterator &operator++() {
        ptr = ptr->getNext();
        return *this;
    }

    ListIterator &operator++(int v) {
        ptr = ptr->getNext();
        return *this;
    }

    ListIterator &operator=(const ListIterator &it) {
        ptr = it.ptr;
        return *this;
    }

    ListIterator &operator=(Element<ValueType> *p) {
        ptr = p;
        return *this;
    }

private:
    Element<ValueType> *ptr;
};

template<class T>
class IteratedLinkedList : public LinkedListParent<T> {
public:
    IteratedLinkedList() : LinkedListParent<T>() {
        cout << "\nIteratedLinkedList constructor";
    }

    virtual ~IteratedLinkedList() { cout << "\nIteratedLinkedList destructor"; }

    ListIterator<T> iterator;

    ListIterator<T> begin() {
        ListIterator<T> it = LinkedListParent<T>::head;
        return it;
    }

    ListIterator<T> end() {
        ListIterator<T> it = LinkedListParent<T>::tail;
        return it;

    }
};

template<class T>
class Queue : public IteratedLinkedList<T> {
public:
    Element<T> *push(T value) override {

        auto new_tail = new Element<T>(value);
        if (this->num == 0) this->head = new_tail;

        if (this->num > 0) {
            this->tail->setNext(new_tail);
            new_tail->setPrevious(this->tail);
        }

        this->tail = new_tail;
        this->num++;
        return new_tail;

    }

    Element<T> *pop() override {
        if (this->num == 0) {
            throw underflow_error("Queue underflow");
        }


        auto ret = this->head;

        if (this->num > 1) {
            auto new_head = this->head->getNext();
            new_head->setPrevious(nullptr);
            this->head = new_head;
        } else {
            this->head = nullptr;
            this->tail = nullptr;
        }

        return ret;
    }
};


template<class T>
class SortedQueue : public Queue<T> {
public:
    Element<T> *push(T value) override {

        auto elem = new Element<T>(value);
        if (this->num == 0) {
            this->head = elem;
            this->tail = elem;
        } else {
            bool is_end = true;
            Element<T> *insert_before = this->getBegin();
            for (; insert_before != nullptr; insert_before = insert_before->getNext()) {
                if (insert_before->getValue() > value) {
                    is_end = false;
                    break;
                }
            }
            if (is_end) {
                this->tail->setNext(elem);
                elem->setPrevious(this->tail);
                this->tail = elem;
            } else if (insert_before == this->head) {
                elem->setNext(this->head);
                this->head->setPrevious(elem);
                this->head = elem;
            } else {

                insert_before->setNext(elem);
                elem->setNext(insert_before);

                auto insert_after = insert_before->getNext();
                elem->setNext(insert_after);
                insert_after->setPrevious(elem);


            }

        }


        this->num++;
        return elem;

    }

    void filter(const std::function<bool(const T &)> predicate) {
        Element<T> *curr = this->getBegin();
        for (; curr != nullptr; curr = curr->getNext()) {
            if (!predicate(curr->getValue())) {
                if (curr == this->head) {
                    this->head = this->head->getNext();
                } else if (curr == this->tail) {
                    this->tail = this->tail->getPrevious();
                } else {
                    curr->getPrevious()->setNext(curr->getNext());
                    curr->getNext()->setPrevious(curr->getPrevious());
                }
            }
        }
    }
};


int main1() {
    list<Fraction> lst;

    for (int i = 0; i < 10; i++)
        lst.push_back(Fraction(i, 10));


/*
 * 27
   Fraction
   Только дроби с числителями, представляющими простые числа.
 *
 * */

    filter<Fraction>(lst, [](Fraction f) -> bool {
        int n = f.get_numerator();
        for (int i = 2; i <= n / 2; ++i) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    });
    print(lst);

    list<Superhero> hero_list;

    auto superman = Superhero("superman", "Unknown", "11.11.2011", true, "", "", 5, 99);
    auto batman = Superhero("batman", "Unknown", "10.10.2010", true, "", "", 4, 100);
    auto catgirl = Superhero("catgirl", "Unknown", "10.10.2010", true, "", "", 0, 5);

    push_descending(hero_list, superman);
    push_descending(hero_list, batman);
    push_descending(hero_list, catgirl);

    cout << "Most powerful hero: " << pop(hero_list) << endl;
    print(hero_list);

    Queue<Superhero> q = Queue<Superhero>();

    q.push(superman);
    q.push(batman);
    q.push(catgirl);

    cout << endl << "Popped: " << q.pop()->getValue() << endl;

    cout << q;


    SortedQueue<Superhero> qs = SortedQueue<Superhero>();

    qs.push(superman);
    qs.push(batman);
    qs.push(catgirl);


    cout << qs;


    return 0;
}

